
<?php
ini_set('display_errors', 'off');
include_once ('registration.php');


$registration = new registration($_POST);

$registrations = $registration->lists();

//var_dump($registrations);
//die();
?>



<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration form</title>
        <link href="Resource/css/bootstrap.min.css" rel="stylesheet">
<!--        <style>
            
            #utility{
                
                float: end;
                
            }
            
        </style>-->

    </head>
    <body>

        <table border='2' class="table table-condensed"> 
            <h2>Registered lists</h2>
            <br/>
            <br/>
            <?php echo $_massage; ?>

            <div><a href="create.php">Add new</a> || <span id="utility">Download as PDF | XLs ||
                    <a href="trashed.php">All Trashed</a></span></div>
            <tr>

                <th>SI.</th>
                <th>ID.</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Birthday</th>
                <th>Email</th>
                <th>Confirm Email</th>
                <th>Phone Number</th>
                <th>City</th>
                <th>Gender</th>
                <th>Religious</th>
                <th>About Me</th>
                <th>Term & Condition</th>
                <th>Action</th>

            </tr>

            <?php
            $sino = 1;
            foreach ($registrations as $registration) {
                ?>
                <tr>

                    <td><?php echo $sino; ?></td>
                    <td> <?php echo $registration->id; ?> </td>
                    <td><?php echo $registration->fname; ?></td>
                    <td><?php echo $registration->lname; ?></td>
                    <td><?php echo $registration->birthday; ?></td>
                    <td><?php echo $registration->email; ?></td>
                    <td><?php echo $registration->cemail; ?></td>
                    <td><?php echo $registration->phonenumber; ?></td>
                    <td><?php echo $registration->city; ?></td>
                    <td><?php echo $registration->gender; ?></td>
                    <td><?php echo $registration->religious; ?></td>
                    <td><?php echo $registration->aboutme; ?></td>
                    <td><?php echo $registration->termandcondition; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $registration->id; ?>"><input class="btn btn-primary btn-sm" type="submit" value="View"/></a> 
                        |  <a class="edit" href="edit.php?id=<?php echo $registration->id; ?>"><input class="btn btn-info btn-sm" type="submit" value="Edit"/></a> 
                        | <a class="delete" href="delete.php?id=<?php echo $registration->id; ?>"><input class="btn btn-danger btn-sm" type="submit" value="Delete"/></a> 
                        | <a class="trash" href="trash.php?id=<?php echo $registration->id; ?>"><input class="btn btn-warning btn-sm" type="submit" value="Trash"/></a>
                    </td>

                </tr>

                <?php
                $sino++;
            }
            ?>

        </table>
        <nav style="float: right; padding-right: 470px">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true"><<</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">>></span>
                    </a>
                </li>
            </ul>


            <script src="https://code.jquery.com/jquery-1.12.0.min.js" type="text/javascript"></script>

            <script>

                $(".delete").bind('click', function (e) {

                    var deleteitem = confirm('Are you sure you want to delete it?');

                    if (!deleteitem) {

                        e.preventDefault();
                    }


                })


            </script>

    </body>
</html>
