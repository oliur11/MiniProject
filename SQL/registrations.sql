-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2016 at 03:35 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `miniproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

CREATE TABLE IF NOT EXISTS `registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `cemail` varchar(255) NOT NULL,
  `phonenumber` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `religious` varchar(255) NOT NULL,
  `aboutme` varchar(255) NOT NULL,
  `termandcondition` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `registrations`
--

INSERT INTO `registrations` (`id`, `fname`, `lname`, `birthday`, `email`, `cemail`, `phonenumber`, `city`, `gender`, `religious`, `aboutme`, `termandcondition`, `deleted_at`) VALUES
(25, 'Proshonjit', 'Pal', '1990-11-18', 'proshonjitpal@gmail.com', 'proshonjitpal@gmail.com', '01739345672', 'Dhaka', 'Male', 'Hinduism', 'I like PC Game\r\n', '', NULL),
(29, 'wetght', 'rh', '2017-02-02', 'wtyjhjg@bcv', 'wtyjhjg@bcv', 'hjg', 'Dhaka', 'Male', 'Islam', '                   Write about you.  \r\n\r\n                ', 'agree', '1452867581'),
(30, 'MD. Ashik ', 'Mahmud', '1992-02-14', 'ashik@gmail.com', 'ashik@gmail.com', '01710331056', 'Dhaka', 'Male', 'Islam', 'I like Design\r\n\r\n                ', 'agree', NULL),
(31, 'MD. Oliur', 'Rahman', '1991-12-01', 'oliur_r14@yahoo.com', 'oliur_r14@yahoo.com', '01738745183', 'Dhaka', 'Male', 'Islam', ' I like Coding\r\n                ', 'agree', NULL),
(32, 'Md. Foysal ', 'Ahmed', '1990-04-29', 'foysal@yahoo.com', 'foysal@yahoo.com', '01923009990', 'Dhaka', 'Male', 'Islam', 'I am a bad boy....\r\n\r\n                ', 'agree', NULL),
(33, 'Md. Kowsar', 'Mahmud', '1990-05-11', 'kowsarmahmud@gmail.com', 'kowsarmahmud@gmail.com', '01687123456', 'Moymonshing', 'Male', 'Islam', 'I like PHP MySQL\r\n\r\n                ', 'agree', NULL),
(34, 'MD.Saikat', 'Paramanik', '1979-08-08', 'saikatparamanik@yahoo.com', 'saikatparamanik@yahoo.com', '01721456557', 'Dhaka', 'Male', 'Islam', 'I like acting....\r\n\r\n                ', 'agree', NULL),
(35, 'Shahin', 'Ferdous', '1985-09-09', 'shahinferdous@gmail.com', 'shahinferdous@gmail.com', '01954565758', 'Dhaka', 'Male', 'Islam', 'I like share business\r\n\r\n                ', 'agree', NULL),
(36, 'rghjmg', 'erhjyt', '2016-01-13', 'retryt@rtyrt', 'sertyjkhjm@fghjnm', '345675u6', 'Borisal', 'Female', 'Islam', '                   Write about you.  \r\n\r\n                ', 'agree', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
