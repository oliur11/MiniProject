<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="Resource/css/bootstrap.min.css" rel="stylesheet">
        
        <style>
            
            #utility{
                
                color: red;
                
            }
            
        </style>
        
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="well">
                    <div style="height: 1200px">
                        <div class="col-sm-3 col-md-3"></div>
                        <div style="padding-top: 40px" class="col-sm-5 col-md-5">    
                            <div class="centered centered">
                                <form action="store.php" method="post"  class="form-horizontal">
                                    <h2>Simple Registration Form</h2>
                                    <br/>
                                    <div>
                                        <label for="fname" >Enter Your First Name<span id='utility'>*</span></label>
                                        <input id="fname" class="form-control" type="text" tabindex="1" name="fname" placeholder="First name" required="required" autofocus="autofocus"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="lname">Enter Your Last Name<span id='utility'>*</span></label>
                                        <input id="lname" class="form-control" type="text" tabindex="2" name="lname" placeholder="Last name" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="birthday">Enter Your Birthday<span id='utility'>*</span></label>
                                        <input id="birthday" class="form-control" type="date" tabindex="3" name="birthday"  required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="email">Enter Your Email Address<span id='utility'>*</span></label>
                                        <input id="email" class="form-control" type="email" tabindex="4" name="email" placeholder="Your Email address" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="cemail">Confirmation Email<span id='utility'>*</span></label>
                                        <input id="cemail" class="form-control" type="email" tabindex="5" name="cemail" placeholder="Confirm Email address" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="phonenumber">Enter Your Phone Number<span id='utility'>*</span></label>
                                        <input id="phonenumber" class="form-control" type="text" tabindex="6" name="phonenumber" placeholder="Your phone number" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="city">Select Your City<span id='utility'>*</span></label>
                                        <select id="city" tabindex="7" required="required" name="city" class="form-control">

                                            <option >__Select One__</option>
                                            <option >Dhaka</option>
                                            <option>Chitagong</option>
                                            <option>Rajshahi</option>
                                            <option>Shylet</option>
                                            <option>Borisal</option>
                                            <option>Khulna</option>
                                            <option>Moymonshing</option>

                                        </select>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="gen">Select your Gender<span id='utility'>*</span></label>
                                        <br/>
                                        <input id="gen" type="radio" tabindex="8" name="gender" value="Male" />Male
                                        <br/>
                                        <input id="gen" type="radio" tabindex="9" name="gender" value="Female" />Female
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="rel">Your Religious<span id='utility'>*</span></label>
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="10" name="religious" value="Islam"  />Islam
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="11" name="religious" value="Christianity" />Christianity
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="12" name="religious" value="Hinduism" />Hinduism
                                    </div>
                                    <br/>
                                    <div>
                                        <label> About Me<span id='utility'>*</span></label>
                                        <br/>
                                        <textarea name="aboutme" class="form-control" rows="3" tabindex="13" placeholder=" Write about you.">
                                        </textarea>

                                    </div>
                                    <br/>
                                    <div>
                                        <input type="checkbox" tabindex="14" name="termandcondition" value="agree" required="required" />
                                        <label>I agree with this term and condition.<span id='utility'>*</span></label>
                                    </div>
                                    <br/>
                                    <br/>
                                    <input id="register-form-" class="btn btn-success" type="submit" tabindex="15" name="submit" value="Submit" /> | <input class="btn btn-danger" type="reset" tabindex="16" name="reset" value="Reset" />
                                    <br/>
                                    <hr/>
                                    <li><a href="lists.php">Go to list</a></li>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--        <script src="https://code.jquery.com/jquery-1.12.0.min.js" type="text/javascript"></script>
<script>

$("#register-form").validate({
rules: {
fname: "required",
lname: "required",
birthday: {
    required: true,
    date: true
},
email: {
    required: true,
    email: true
},
cemail: {
    required: true,
    email: true
},
phonenumber:{ "required"
},
city: {"required"
},
gender:{ "required"
},
religious:{ "required"
},
aboutme: {"required"
},
termandcondition: "required"
}
messages: {
fname: "Please enter your firstname",
lname: "Please enter your lastname",
birthday: {
    required: "Please provide a birthday",
    
},
email:{ "Please enter a valid email address",

},
cemail: "Please enter a valid email address",
agree: "Please accept our policy"
},
phonenumber:{ "Please enter your phone number"
},
city:{ "Please select your city"
},
gender:{ "Please select your gender"
},
religious:{ "Please select your religious"
},
aboutme: {"Please write about you"
},
termandcondition: {"Please accept our policy"
},
submitHandler: function(form) {
form.submit();
}
});


</script>    
        -->
    </body>
</html>
