
<?php
//var_dump($_POST);
//die();
include_once ('registration.php');


$registration = new registration();

$registrations = $registration->show($_REQUEST['id']);
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="Resource/css/bootstrap.min.css" rel="stylesheet">
        
         <style>
            
            #utility{
                
                color: red;
                
            }
            
        </style>
        
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="well">
                    <div style="height: 1200px">
                        <div class="col-sm-3 col-md-3"></div>
                        <div style="padding-top: 40px" class="col-sm-5 col-md-5">    
                            <div class="centered centered">

                                <form action="update.php" method="get" class="form-horizontal">
                                    <h2>Simple Registration Form</h2>
                                    <br/>
                                    <input type="hidden" name="id" value="<?php echo $registrations->id ?>"/>
                                    <div>
                                        <label for="fname">Edit Your First Name<span id='utility'>*</span></label>
                                        <input id="fname" class="form-control" type="text" tabindex="1" name="fname" placeholder="First name" value="<?php echo $registrations->fname ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="lname">Edit Your Last Name<span id='utility'>*</span></label>
                                        <input id="lname" class="form-control" type="text" tabindex="2" name="lname" placeholder="Last name" value="<?php echo $registrations->lname ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="birthday">Edit Your Birthday<span id='utility'>*</span></label>
                                        <input id="birthday" class="form-control" type="date" tabindex="3" name="birthday" value="<?php echo $registrations->birthday ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="email">Edit Your Email Address<span id='utility'>*</span></label>
                                        <input id="email" class="form-control" type="email" tabindex="4" name="email" placeholder="Your Email address" 
                                               value="<?php echo $registrations->email ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="cemail">Edit Confirmation Email<span id='utility'>*</span></label>
                                        <input id="cemail" class="form-control" type="email" tabindex="5" name="cemail" placeholder="Confirm Email address" 
                                               value="<?php echo $registrations->cemail ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="phonenumber">Edit Your Phone Number<span id='utility'>*</span></label>
                                        <input id="phonenumber" class="form-control" type="number" tabindex="6" name="phonenumber" placeholder="Your phone number" 
                                               value="<?php echo $registrations->phonenumber ?>" required="required"/>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="city">Edit Your City<span id='utility'>*</span></label>
                                        <select id="city" class="form-control" tabindex="7" required="required" name="city" value='<?php echo $registrations->city ?>'>

                                            <option >__Select One__</option>
                                            <option >Dhaka</option>
                                            <option>Chitagong</option>
                                            <option>Rajshahi</option>
                                            <option>Shylet</option>
                                            <option>Borisal</option>
                                            <option>Khulna</option>
                                            <option>Moymonshing</option>

                                        </select>
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="gen">Edit your Gender<span id='utility'>*</span></label>
                                        <br/>
                                        <input id="gen" type="radio" tabindex="8" name="gender" required="required" value="<?php echo $registrations->gender ?>" />Male
                                        <br/>
                                        <input id="gen" type="radio" tabindex="9" name="gender" required="required" value="<?php echo $registrations->gender ?>" />Female
                                    </div>
                                    <br/>
                                    <div>
                                        <label for="rel">Edit Your Religious<span id='utility'>*</span></label>
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="10" name="religious" required="required" value="<?php echo $registrations->religious ?>"  />Islam
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="11" name="religious" required="required" value="<?php echo $registrations->religious ?>" />Christianity
                                        <br/>
                                        <input id="rel" type="checkbox" tabindex="12" name="religious" required="required" value="<?php echo $registrations->religious ?>" />Hinduism
                                    </div>
                                    <br/>
                                    <div>
                                        <label>Edit About Me<span id='utility'>*</span></label>
                                        <br/>
                                        <textarea name="aboutme" class="form-control" rows="3"  tabindex="13" >
                                            <?php echo $registrations->aboutme ?>

                                        </textarea>

                                    </div>
                                    <br/>
                                    <div>
                                        <input for="term" type="checkbox" tabindex="14" name="termandcondition" placeholder="" value="<?php echo $registrations->termandcondition ?>" required="required" />
                                        <label id="term"> I agree with this term and condition.<span id='utility'>*</span></label>
                                    </div>
                                    <br/>
                                    <br/>
                                    <input type="submit" class="btn btn-success" tabindex="15" name="submit" value="Submit" /> | <input type="reset" class="btn btn-danger" tabindex="16" name="reset" value="Reset" />
                                    <br/>
                                    <hr/>
                                    <li><a href="lists.php">Go to list</a></li>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
